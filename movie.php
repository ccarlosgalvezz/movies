<!DOCTYPE html>
<html lang="en">
    <?php include 'includes/header.php';

        $id = $_REQUEST['id'];

        require 'includes/connection_open.php';
    
        $sql = "
            SELECT * FROM movies m LEFT JOIN categories c ON m.category_id = c.id WHERE m.id LIKE $id; 
        ";
        $query = mysqli_query($conn, $sql);
    
        if(!$query) {
            echo "Error. Codigo:" . mysqli_connect_errno() . "<br>";
        }

        $row = mysqli_fetch_assoc($query);
        ?>

        <section class="main">

            <?php echo "<h2>" . $row["title"] . "</h2>"; ?>
            
            <div class="container movie">
                <?php
                    echo '<img src="' . $row['image'] . '">';
                    echo '<div><b>Género:</b> ' . $row["name"] . '<br>';
                    echo '<b>Sinopsis:</b> ' . $row["excerpt"] . '</div>';
                    ?>
            </div>
            
            <div class="comments">
                <?php include 'includes/comments.php' ?>
            </div>
            
        </section>
        <?php
            mysqli_close($conn);
            include 'includes/footer.php'; 
        ?>
</html>