<footer class="footer">
        <script>
            $(document).ready(function(){
                $('body').on('click', '.btn-filter', function(e) {
                    e.preventDefault();
                    var genre = $(this).attr('data-genre');
                    $('.btn-filter').removeClass('btn-on');
                    $(this).addClass('btn-on');
                    filter(genre);
                });
            });
        
            function filter(genre){
                if(genre == '0'){
                    $('.movies').show();
                } else {
                    $('.movies').hide();
                    $('.' + genre).show();
                }
            }
        </script>
</footer>
</body>