<?php
	// mostrar comentarios
	$sql = "
		SELECT * FROM comments WHERE movie_id LIKE $id; 
	";
	$query = mysqli_query($conn, $sql);

	if(!$query) {
		echo "Error. Codigo:" . mysqli_connect_errno() . "<br>";
	}

	$comments = mysqli_num_rows($query);

	// enviar comentarios
	if (isset($_POST['send'])) {
		$content = $_POST['content']; 
		$sql2 = "
			INSERT INTO comments VALUES(null,'".$content."','".$id."',NOW()); 
		";

		$query2 = mysqli_query($conn, $sql2);

		if(!$query2) {
			echo "Error al enviar, inténtelo de nuevo. Codigo:" . mysqli_connect_errno() . "<br>";
		} else {
			echo "<h3>¡Gracias por tu comentario!</h3>";
			header("Refresh: 2;"); 
			die();
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Comentarios</title>
</head>
<body>
	<form name="comment" method="POST" action="">
		<fieldset>
			<legend>Comentarios</legend>
			<?php
			if ($comments > 0) {
				while ($row = mysqli_fetch_assoc($query)) {
					?>
					<div class="comment">
					<?php
						$date = date_create($row['date']);
						echo '<span style="font-style: italic;">"' . $row['content'] . '"</span><br>';
						echo ' ('.date_format($date,"d/m/Y").')';
					?>
					</div>
					<?php
				}
			} else {
				?>
					<div class="container">
						<?php
						echo 'Aún sin comentarios';
						?>
					</div>
				<?php
			}
			?>

            <textarea name="content" rows="8" placeholder="Escribe tu comentario"></textarea>
            <input type="submit" name="send" value="Enviar">
		</fieldset>
	</form>
</body>
</html>