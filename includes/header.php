<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Movies by Carlos Gálvez</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"  crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
</head>
<body>
    <header class="header">
        <h1>PELÍCULAS RECOMENDADAS</h1>
        <?php 
            if($_REQUEST) {
                echo '<a href="index.php" style="font-size: 1rem; float: right; padding: 0 20px; background-color: rgba(0,0,0,.4);"><i class="fas fa-angle-double-left"></i> Volver</a>';
            }
        ?>
    </header>
    