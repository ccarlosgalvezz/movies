-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para carlos_galvez
CREATE DATABASE IF NOT EXISTS `carlos_galvez` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `carlos_galvez`;

-- Volcando estructura para tabla carlos_galvez.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla carlos_galvez.categories: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
REPLACE INTO `categories` (`id`, `name`) VALUES
	(1, 'comedia'),
	(2, 'acción'),
	(3, 'drama'),
	(4, 'musical');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Volcando estructura para tabla carlos_galvez.comments
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `movie_id` int(11) unsigned NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `movie_fk` (`movie_id`),
  CONSTRAINT `movie_fk` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla carlos_galvez.comments: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
REPLACE INTO `comments` (`id`, `content`, `movie_id`, `date`) VALUES
	(1, 'La mejor película de la historia', 1, '2023-03-29 07:42:13'),
	(2, 'Una obra de arte', 1, '2023-03-29 08:02:36'),
	(3, 'La tenéis clasificada como comedia pero en realidad es un drama, no entiendo como un portal tan serio como este comete este tipo de errores.', 1, '2023-03-29 08:04:50'),
	(4, 'Sublime', 3, '2023-03-29 08:08:42'),
	(5, 'Es una obra maestra de realismo estilizado -- el estilo viene de la manera en la que la fotografía y el montaje hacen observaciones sobre la esencia del contenido', 3, '2023-03-29 08:09:42'),
	(6, 'Muy pocas películas abordan el arte de la discusión como tema, sin duda nos vendrían bien algunas más, pero hasta entonces, la ventana de Lumet al tenso deber cívico continuará sirviendo con mucha fuerza', 3, '2023-03-29 08:09:49'),
	(7, 'Aunque series como esta, construidas con honestidad y con mucho trabajo por parte de gente con mucho talento, no sean de mi agrado (...) eso no impide que pueda disfrutar sus aspectos más destacados', 4, '2023-03-29 08:10:18'),
	(8, 'No me gustó nada', 4, '2023-03-29 08:35:30');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Volcando estructura para tabla carlos_galvez.movies
CREATE TABLE IF NOT EXISTS `movies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_fk` (`category_id`),
  CONSTRAINT `category_fk` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla carlos_galvez.movies: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `movies` DISABLE KEYS */;
REPLACE INTO `movies` (`id`, `category_id`, `title`, `excerpt`, `image`) VALUES
	(1, 1, 'El padrino', 'América, años 40. Don Vito Corleone (Marlon Brando) es el respetado y temido jefe de una de las cinco familias de la mafia de Nueva York. Tiene cuatro hijos: Connie (Talia Shire), el impulsivo Sonny (James Caan), el pusilánime Fredo (John Cazale) y Michael (Al Pacino), que no quiere saber nada de los negocios de su padre. Cuando Corleone, en contra de los consejos de "Il consigliere" Tom Hagen (Robert Duvall), se niega a participar en el negocio de las drogas, el jefe de otra banda ordena su asesinato. Empieza entonces una violenta y cruenta guerra entre las familias mafiosas.', 'https://pics.filmaffinity.com/the_godfather-488102675-msmall.jpg'),
	(2, 2, 'Planeta Tierra II', 'La segunda parte del mítico documental "Planeta Tierra", nos sumerge en increíbles paisajes y nos permite experimentar el mundo a través de la perspectiva de los animales. Viajando a través de junglas, desiertos, montañas, islas, dehesas y ciudades, esta serie documental explora las características de los habitantes más icónicos de la tierra y sus extraordinarios modos de supervivencia. La nueva tecnología nos acerca a detalles sin precedentes en la vida de los animales.', 'https://pics.filmaffinity.com/planet_earth_ii-875634466-msmall.jpg'),
	(3, 2, 'The Wire', 'En los barrios bajos de Baltimore, se investiga un asesinato relacionado con el mundo de las drogas. Un policía es el encargado de detener a los miembros de un importante cártel. La corrupción policial, las frágiles lealtades dentro de los cárteles y la miseria vinculada al narcotráfico son algunos de los problemas denunciados en esta serie.', 'https://pics.filmaffinity.com/the_wire-680717276-msmall.jpg'),
	(4, 4, 'Doce hombres sin piedad', 'Los doce miembros de un jurado deben juzgar a un adolescente acusado de haber matado a su padre. Todos menos uno están convencidos de la culpabilidad del acusado. El que disiente intenta con sus razonamientos introducir en el debate una duda razonable que haga recapacitar a sus compañeros para que cambien el sentido de su voto. ', 'https://pics.filmaffinity.com/12_angry_men-122099105-msmall.jpg');
/*!40000 ALTER TABLE `movies` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
