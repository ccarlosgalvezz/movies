<!DOCTYPE html>
<html lang="en">
    <?php include 'includes/header.php'; ?>
    
        <section class="buttons">
            <button type="button" data-genre="0" class="0 btn-filter" style="width: auto;">TODAS</button>
            <?php
                require 'includes/connection_open.php';
            
                $sql = "
                    SELECT * FROM categories; 
                ";
                $query = mysqli_query($conn, $sql);
            
                $genre = mysqli_num_rows($query);
                if ($genre > 0) {
                    while ($row = mysqli_fetch_assoc($query)) {
                        echo "
                        <button type='button' data-genre='". $row['id'] ."' class='". $row['id'] ." btn-filter' style='width: auto;'>". $row['name'] ."</button>
                        ";
                    }
                } 
            ?>
        </section>
        
        <section class="main">
            <?php
                $sql = "
                    SELECT * FROM movies; 
                ";
                $query = mysqli_query($conn, $sql);
            
                if(!$query) {
                    echo "Error. Codigo:" . mysqli_connect_errno() . "<br>";
                }

                $movies = mysqli_num_rows($query);
                if ($movies > 0) {
                ?>
                    <div class="container">
                        <?php
                            while ($row = mysqli_fetch_assoc($query)) {
                                ?>
                                <a href="movie.php?id=<?php echo $row['id'] ?>" class="movies <?php echo $row['category_id'] ?>">
                                <?php
                                    echo '<img src="' . $row['image'] . '">';
                                    echo '<div><span class="title">' . $row['title'] . '</span><br>';
                                    echo $row['excerpt'] . '</div>';
                                ?>
                                </a>
                                <?php
                            }
                        ?>
                    </div>
                <?php
                } else {
                ?>
                    <div class="container">
                        <?php
                        echo '<h2>Ningún resultado</h2>';
                        ?>
                    </div>
                <?php
                }
                mysqli_close($conn);
            ?>
        </section>

        <?php include 'includes/footer.php'; ?>

</html>